PROGRAM analysis

USE constants_mod
USE fft_mod
USE readfile_mod
IMPLICIT NONE

INCLUDE 'mpif.h'

!##########################################################################################

INTEGER, PARAMETER :: ndumps = 20

REAL*4, ALLOCATABLE :: fx(:,:,:,:), fft_in(:,:,:)
REAL*4, ALLOCATABLE :: vxk_re(:,:,:), vxk_im(:,:,:),vyk_re(:,:,:), vyk_im(:,:,:),vzk_re(:,:,:), vzk_im(:,:,:)
REAL*4 :: Lx, Ly, Lz, dx
REAL*4 :: x, y, z, rho
REAL*4 :: total_ke, v_rms
INTEGER :: nmax, i, j, k, nd, mem_bytes
REAL*4 :: t1, t2, t3, t4

!##########################################################################################
ALLOCATE(vxk_re(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z))
ALLOCATE(vxk_im(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z))
ALLOCATE(vyk_re(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z))
ALLOCATE(vyk_im(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z))
ALLOCATE(vzk_re(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z))
ALLOCATE(vzk_im(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z))
ALLOCATE(fx(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z,4))
ALLOCATE(fft_in(1:nx*nranks_x,1:ny*nranks_y,1:nz*nranks_z))

mem_bytes = SIZEOF(vxk_re) + SIZEOF(vxk_im) + SIZEOF(vyk_re) + SIZEOF(vyk_im) + SIZEOF(vzk_re) + SIZEOF(vzk_im) +&
            SIZEOF(fx)
 
PRINT*,''
PRINT*,'Memory allocated for work arrays (Mb) = ', mem_bytes*1.e-6
PRINT*,''
!##########################################################################################

OPEN(UNIT=3, FILE='Output/avgs.dat', FORM = 'UNFORMATTED', ACCESS = 'STREAM')


! grid size
Lx = 1.d0
Ly = 1.d0
Lz = 1.d0
dx = Lx/DBLE(nranks_x*nx)
nmax = MIN(nranks_x*nx,nranks_y*ny,nranks_z*nz)


! initialize fft module
CALL fft_init(nranks_x*nx,nranks_y*ny,nranks_z*nz)

! loop over file dumps

DO nd = 0, ndumps


    t1 = MPI_WTIME()
    !CALL readfile_ranks_singlevar(0, 1, 8, .FALSE., fx)
    CALL readfile_ranks_multivar(nd, 1, 1, 4, .FALSE., fx)
    t2 =  MPI_WTIME()


    CALL compute_ke()
    CALL compute_vrms()

    WRITE(3) SNGL(nd),total_ke,v_rms

    ! convert momentum into velocity
    DO k = 1, nranks_z*nz
    DO j = 1, nranks_y*ny
    DO i = 1, nranks_x*nx

        rho = fx(i,j,k,1)
        fx(i,j,k,2:4) = fx(i,j,k,2:4)/rho  

    END DO
    END DO
    END DO


    PRINT*,'Performing FFT.'

    t3 = MPI_WTIME()

    ! perform FFTs on velocity field
    PRINT*,'FFT(vx)'
    fft_in(:,:,:) = fx (:,:,:,2) 
    CALL fft_3d(fft_in, vxk_re, vxk_im)
    PRINT*,'FFT(vy)'
    fft_in(:,:,:) =fx (:,:,:,3) 
    CALL fft_3d(fft_in, vyk_re, vyk_im)
    PRINT*,'FFT(vz)'
    fft_in(:,:,:) = fx (:,:,:,4) 
    CALL fft_3d(fft_in, vzk_re, vzk_im)

    t4 = MPI_WTIME()

    PRINT*,'Computing velocity power spectrum.'
    CALL compute_velocity_pk(nd)

    PRINT*,''
    PRINT*,'File read time (sec) = ', t2-t1
    PRINT*,'FFT time (sec) = ', t4-t3
    PRINT*,''

END DO





!OPEN(UNIT=12, FILE='output_3d.dat', FORM = 'UNFORMATTED', ACCESS = 'STREAM')

!DO iz = 1, nz
!DO iy = 1, ny
!DO ix = 1, nx
!    WRITE(12) fx(ix,iy,iz,1), fk_re(ix,iy,iz,1), fk_im(ix,iy,iz,1)
!END DO
!END DO
!END DO

CLOSE(UNIT=3)

DEALLOCATE(fx, vxk_re, vxk_im, vyk_re, vyk_im, vzk_re, vzk_im, fft_in)




PRINT*,'Done.'
PRINT*,''


!##########################################################################################


CONTAINS


SUBROUTINE compute_velocity_pk(t_dump)

    INTEGER, INTENT(IN) :: t_dump 
    INTEGER :: kx, ky, kz, kbins, ik
    REAL*4 :: kmin, kmax, dk, k, k0, vk_sqr
    REAL*4, ALLOCATABLE :: Pk(:) 
    CHARACTER(LEN=300) :: filename
    CHARACTER(LEN=6) :: uniti


    IF(t_dump<10) THEN
        WRITE(uniti,'(I1.1)') t_dump
    ELSE IF(t_dump>=10 .and. t_dump<100) THEN
        WRITE(uniti,'(I2.2)') t_dump
    ELSE IF(t_dump>=100 .and. t_dump<1000) THEN
        WRITE (uniti,'(I3.3)') t_dump
    ELSE IF(t_dump>=1000 .and. t_dump<10000) THEN
        WRITE (uniti,'(I4.3)') t_dump
    ELSE IF(t_dump>=10000 .and. t_dump<100000) THEN
        WRITE (uniti,'(I5.3)') t_dump  
    END IF
    
    
    ! set number of bins (can't exceed nx/2 )
    kbins = nx*nranks_x/2
    
    ALLOCATE(Pk(1:kbins))
    Pk = 0.d0
    
    ! k band parameters
    k0 = TWOPI/Lx
    kmin = 0.d0
    kmax = 2.d0*k0*(nx*nranks_x/2)    
    dk = (kmax-kmin)/kbins ! uinform bin width
    
    ! loop over k space grid and deposit velocity fourier amplitudes (squared) into power spectrum bins (i.e. shells in k-space)
    DO kz = 0, -1+nmax/2
        DO ky = 0, -1+nmax/2
            DO kx = 0, -1+nmax/2
    
                k = k0 * SQRT(DBLE(kx**2 + ky**2 + kz**2))
                !vk_sqr = 2.d0 * (vxk_re(kx+nmax/2,ky+nmax/2,kz+nmax/2)**2 + vxk_im(kx+nmax/2,ky+nmax/2,kz+nmax/2)**2 + &
                !         vyk_re(kx+nmax/2,ky+nmax/2,kz+nmax/2)**2 + vyk_im(kx+nmax/2,ky+nmax/2,kz+nmax/2)**2 + &
                !         vzk_re(kx+nmax/2,ky+nmax/2,kz+nmax/2)**2 + vzk_im(kx+nmax/2,ky+nmax/2,kz+nmax/2)**2 )


                vk_sqr = vxk_re(kx+1+nmax/2,ky+1+nmax/2,kz+1+nmax/2)**2 + vxk_im(kx+1+nmax/2,ky+1+nmax/2,kz+1+nmax/2)**2 + &
                         vyk_re(kx+1+nmax/2,ky+1+nmax/2,kz+1+nmax/2)**2 + vyk_im(kx+1+nmax/2,ky+1+nmax/2,kz+1+nmax/2)**2 + &
                         vzk_re(kx+1+nmax/2,ky+1+nmax/2,kz+1+nmax/2)**2 + vzk_im(kx+1+nmax/2,ky+1+nmax/2,kz+1+nmax/2)**2 + &
                         vxk_re(-kx+1+nmax/2,-ky+1+nmax/2,-kz+1+nmax/2)**2 + vxk_im(-kx+1+nmax/2,-ky+1+nmax/2,-kz+1+nmax/2)**2 + &
                         vyk_re(-kx+1+nmax/2,-ky+1+nmax/2,-kz+1+nmax/2)**2 + vyk_im(-kx+1+nmax/2,-ky+1+nmax/2,-kz+1+nmax/2)**2 + &
                         vzk_re(-kx+1+nmax/2,-ky+1+nmax/2,-kz+1+nmax/2)**2 + vzk_im(-kx+1+nmax/2,-ky+1+nmax/2,-kz+1+nmax/2)**2
                         
                IF(kx .EQ. 0 .AND. ky .EQ. 0 .AND. kz .EQ. 0) vk_sqr = 0.5d0 * vk_sqr
    
                ik = 1 + INT(k/dk)

                Pk(ik) = Pk(ik) + vk_sqr
    
            END DO
        END DO
    END DO
    
    Pk = Pk * (dx**3)
    
    !DO ik = 1, kbins
    !    PRINT*,'k, Pk = ',(ik-0.5)*dk,Pk(ik)
    !END DO
    
    ! dump power spectrum into file
    filename = TRIM('Output/velocity_Pk_dump=')//TRIM(uniti)//TRIM('.dat')
    OPEN(UNIT=1, FILE=filename, FORM = 'UNFORMATTED', ACCESS = 'STREAM')
    
    DO ik = 1, kbins
        WRITE(1) (ik-0.5)*dk,Pk(ik)
    END DO
    
    CLOSE(UNIT=1)
    
    DEALLOCATE(Pk)


END SUBROUTINE compute_velocity_pk


SUBROUTINE compute_ke()

    INTEGER :: i, j, k
    REAL(4) :: total_ke, rho 

    total_ke = 0.d0

    DO k = 1, nranks_z*nz
    DO j = 1, nranks_y*ny
    DO i = 1, nranks_x*nx

        rho = fx(i,j,k,1) 
        total_ke = total_ke + (fx(i,j,k,2)**2 + fx(i,j,k,3)**2 + fx(i,j,k,4)**2) /rho  

    END DO
    END DO
    END DO

    total_ke = 0.5d0 * total_ke * (dx**3)

    PRINT*,'Kinetic Energy = ',total_ke 


END SUBROUTINE compute_ke




SUBROUTINE compute_vrms()

    INTEGER :: i,j,k
    REAL(4) :: rho 

    v_rms = 0.d0

    DO k = 1, nranks_z*nz
    DO j = 1, nranks_y*ny
    DO i = 1, nranks_x*nx

        rho = fx(i,j,k,1) 
        v_rms = v_rms + (fx(i,j,k,2)**2 + fx(i,j,k,3)**2 + fx(i,j,k,4)**2) /(rho**2)  

    END DO
    END DO
    END DO

    v_rms = SQRT(v_rms) * (dx**3)

    PRINT*,'rms velocity = ',v_rms 


END SUBROUTINE compute_vrms



END PROGRAM analysis